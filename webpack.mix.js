const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css')
	.styles("resources/assets/css/main.css", 'public/css/main.css')
	.scripts([
	    'resources/assets/js/main.js'
	], 'public/js/all.js')
	.browserSync({
		proxy: '127.0.0.1:8000'
	});

mix.webpackConfig({
	module: {
		rules: [
			{
				test: /\.pug$/,
				oneOf: [
					{
						resourceQuery: /^\?vue/,
						use: ['pug-plain-loader']
					},
					{
						use: ['raw-loader', 'pug-plain-loader']
					}
				]
			}
		]
	}
});
