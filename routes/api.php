<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/task/', 'Task\TaskController@getJson');
Route::post('/task', 'Task\TaskController@addTask');
Route::delete('/task/{id}', 'Task\TaskController@deleteTask');

Route::get('/chat/messages', 'Admin\Chat\ChatsController@fetchMessages');
Route::post('/chat/messages', 'Admin\Chat\ChatsController@sendMessage');