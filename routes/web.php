<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/task/', 'Task\TaskController@show_all');
    Route::get('/task/{id}', 'Task\TaskController@show_one');
    Route::post('/task', 'Task\TaskController@add_task');
    Route::delete('/task/{id}', 'Task\TaskController@delete_task');
    Route::get('/home', 'HomeController@index');
    Route::get('/admin', 'Admin\AdminController@index');
    Route::get('/admin/goods/', 'Admin\Goods\GoodController@show_all');
    Route::get('/admin/goods/add/', 'Admin\Goods\GoodController@form_add_goods');
    Route::get('/admin/goods/{id}', 'Admin\Goods\GoodController@update');
    Route::post('/admin/goods', 'Admin\Goods\GoodController@add_good');
    Route::delete('/admin/goods/{id}', 'Admin\Goods\GoodController@delete_good');
    Route::get('/broadcast', 'Broadcast\BroadcastController@index');

Route::get('/admin/chat/', 'Admin\Chat\ChatsController@index');
