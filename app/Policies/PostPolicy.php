<?php

namespace App\Policies;

use App\User;
use App\Good;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function view(User $user, Good $good)
    {
        //
    }

    /**
     * Determine whether the user can create goods.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function update(User $user, Good $good)
    {
        //
    }

    /**
     * Determine whether the user can delete the good.
     *
     * @param  \App\User  $user
     * @param  \App\Good  $good
     * @return mixed
     */
    public function delete(User $user, Good $good)
    {
        //
    }
}
