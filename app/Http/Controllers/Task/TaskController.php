<?php

namespace App\Http\Controllers\Task;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task as Task;
use Validator;

class TaskController extends Controller
{
     

    /**
     * 
     *
     * 
     */
    public function show_one($id)
    {
		$task = Task::find($id)->get();

    	return view('task.task', [
            'task' => $task
        ]);
    } 
    /**
     * 
     * Get Task for Vue 
     * 
     */
    public function getJson()
    {
        return Task::orderBy('created_at', 'asc')->get()->toJson();
    } 
    /**
     * Add New Task
     *
     * @return \Illuminate\Http\Response
     */
    public function add_task(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $task = new Task;
        $task->name = $request->name;
        $task->save();

        return redirect('/admin/');
    } 
    /**
     * Delete Task
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_task($id)
    {
        Task::findOrFail($id)->delete();

        return redirect('/admin');
    }
    /**
     * 
     * Get Task for Vue.js
     * 
     */
    public function show_all()
    {
        return view('tasks', [
            'tasks' => Task::orderBy('created_at', 'asc')->get()
        ]);
    } 
    /**
     * Add New Task for Vue.js
     *
     * @return array
     */
    public function addTask(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $task = new Task;
        $task->name = $request->name;
        $task->save();

        $insertedId = $task->id;

        return ["success"=>"OK","id"=>$insertedId,"newTask"=>Task::find($insertedId)->toJson()];
    } 

    /**
     * Delete Task for Vue.js
     *
     * @return array
     */
    public function deleteTask($id)
    {
        Task::findOrFail($id)->delete();

        return ["success"=>"OK","tasks"=>Task::orderBy('created_at', 'asc')->get()->toJson()];
    }
}
