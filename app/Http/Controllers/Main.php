<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task as Task;

class Main extends Controller
{
        
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('tasks', [
            'tasks' => Task::orderBy('created_at', 'asc')->get()
        ]);
    }
}
