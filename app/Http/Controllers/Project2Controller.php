<?php

namespace App\Http\Controllers;

use App\Project2;
use Illuminate\Http\Request;

class Project2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project2  $project2
     * @return \Illuminate\Http\Response
     */
    public function show(Project2 $project2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project2  $project2
     * @return \Illuminate\Http\Response
     */
    public function edit(Project2 $project2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project2  $project2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project2 $project2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project2  $project2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project2 $project2)
    {
        //
    }
}
