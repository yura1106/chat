<?php

namespace App\Http\Controllers;

use App\PhotoController;
use Illuminate\Http\Request;

class PhotoControllerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PhotoController  $photoController
     * @return \Illuminate\Http\Response
     */
    public function show(PhotoController $photoController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PhotoController  $photoController
     * @return \Illuminate\Http\Response
     */
    public function edit(PhotoController $photoController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PhotoController  $photoController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhotoController $photoController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PhotoController  $photoController
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhotoController $photoController)
    {
        //
    }
}
