<?php

namespace App\Http\Controllers;

use DummyFullModelClass;
use App\Main;
use Illuminate\Http\Request;

class Test extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function index(Main $main)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function create(Main $main)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Main  $main
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Main $main)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Main  $main
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function show(Main $main, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Main  $main
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function edit(Main $main, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Main  $main
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Main $main, DummyModelClass $DummyModelVariable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Main  $main
     * @param  \DummyFullModelClass  $DummyModelVariable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Main $main, DummyModelClass $DummyModelVariable)
    {
        //
    }
}
