<?php

namespace App\Http\Controllers\Admin\Chat;

use App\Message as Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Events\MessageSent;
use Illuminate\Support\Facades\Log;
use Validator;

class ChatsController extends Controller
{
	public function __construct()
	{
	  // $this->middleware('auth');
	}

	/**
	 * Show chats
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	  return view('admin.chat.index');
	}

	/**
	 * Fetch all messages
	 *
	 * @return Message
	 */
	public function fetchMessages()
	{
	  return Message::orderBy('created_at', 'asc')->get();
	}

	/**
	 * Persist message to database
	 *
	 * @param  Request $request
	 * @return Response
	 */
	public function sendMessage(Request $request)
	{
  		$user = Auth::user();

  		$addMessage = $request->input('message');

        $message = new Message;
        $message->user = $addMessage['user_name'];
        $message->message = $addMessage['message'];
        $message->save();
//428112516:AAHQqfptamb7bRALOGqf74YRXbEdTitcsmM
 
      	$telegramToken = "459930837:AAG6gO9MoGbzm7pxBX-o8xAjhwhZM-QSaU4";
      	$chatId = 402564046;
        $postdata = http_build_query(
		    array(
		        'chat_id' => $chatId,
		        'text' => $addMessage['message']
		    )
		);

		$opts = array('http' =>
		    array(
		        'method'  => 'POST',
		        'header'  => 'Content-type: application/x-www-form-urlencoded',
		        'content' => $postdata
		    )
		);
		$context  = stream_context_create($opts);
//		$result = file_get_contents('https://api.telegram.org/bot'.$telegramToken.'/sendMessage', false, $context);

		broadcast(new MessageSent($message));

        return response()->json([
        	'message' => $message
        ]);

	}

}
