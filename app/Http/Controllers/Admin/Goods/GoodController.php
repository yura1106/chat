<?php

namespace App\Http\Controllers\Admin\Goods;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Good as Good;
use Log;
use Validator;

class GoodController extends Controller
{    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
        
    public function show_one($id)
    {
		$good = Good::find($id)->get();

    	return view('admin.goods.index', [
            'good' => $good
        ]);
    } 
    /** 
     * 
     */
    public function show_all()
    {

        $goods = Good::orderBy('created_at', 'asc')->paginate(5);

    	return view('admin.goods.index', [
            'goods' => $goods
        ]);
    } 
    /** 
     * 
     */
    public function form_add_goods()
    {
        return view('admin.goods.add.index', array());
    } 
    /**
     * Add New Good
     *
     * @return \Illuminate\Http\Response
     */
    public function add_good(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/admin/goods/add/')
                ->withInput()
                ->withErrors($validator);
        }
        $good = new Good;
        $good->name = $request->name;
        $good->save();
        
        return response()->json([
            'message' => 'Ok',
        ]);
        //return redirect('/admin/goods/');
    } 

    /**
     * Delete Good
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_good($id)
    {
        Good::findOrFail($id)->delete();

        return redirect('/admin/goods/');
    }
}
