@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Good
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <!-- New Good Form -->
                    <form action="/admin/goods" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <!-- Good Name -->
                        <div class="form-group">
                            <label for="good-name" class="col-sm-3 control-label">Good</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="good-name" class="form-control" value="{{ old('good') }}">
                            </div>
                        </div>

                        <!-- Add Good Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Good
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
