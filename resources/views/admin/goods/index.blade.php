@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">

            <!-- Current Tasks -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        All Goods
                    </div>

                    <div class="panel-body">
                    	<div class="row">
                        <table class="table table-striped task-table">
                            <thead>
                                <th>Good</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($goods as $good)
                                    <tr>
                                        <td class="table-text"><div>{{ $good->name }}</div></td>

                                        <!-- Good Delete Button -->
                                        <td>
                                            <form action="/admin/goods/{{ $good->id }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                         {!! $goods->render() !!}
                        </div>
                        <div class="row">
                        	<div class="col-xs-12">
                                <!-- Display Validation Errors -->
                                @include('common.errors')

                                <!-- New Good Form -->
                                <form action="/admin/goods" method="POST" class="form-horizontal" id="formGood">
                                    {{ csrf_field() }}

                                    <!-- Good Name -->
                                    <div class="form-group">
                                        <label for="good-name" class="col-sm-3 control-label">Good</label>

                                        <div class="col-sm-6">
                                            <input type="text" name="name" id="good-name" class="form-control" value="{{ old('good') }}">
                                        </div>
                                    </div>


                                    <!-- Add Good Button -->
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <button type="submit" class="btn btn-default" id="addGood">
                                                <i class="fa fa-btn fa-plus"></i>Add Good
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
