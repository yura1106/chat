
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Chat from './components/Chat.vue';
import ChatForm from './components/ChatForm.vue';
import Tasks from './components/Tasks.vue';

Vue.component('example', require('./components/Example.vue'));
Vue.component('tasks', Tasks);
Vue.component('chat', Chat);
Vue.component('chat-form', ChatForm);

const app = new Vue({
    el: '#app',

    data: {
        'messages':[]
    },
    created () {
        this.fetchMessages();

        let timer;
        Echo.private('chat')
            .listen('MessageSent', (e) => {
                console.log('Echo',e);
                this.messages.push({
                    message: e.message.message,
                    user: e.message.user
                });
            })
            .listenForWhisper('typing', (e) => {
                if(typeof timer != 'undefined') clearTimeout(timer);
                $('#user-typed').html(e.name + ' is typing...');
                timer = setTimeout(()=>{
                    $('#user-typed').html('');
                }, 1000);
                console.log(e.name);
            });
    },

    methods: {
        fetchMessages() {
            axios.get('/api/chat/messages').then(response => {
                this.messages = response.data;
            });
        },
        addMessage(message,user_name) {
            if(message !== '' && user_name !== ''){
                axios.post('/api/chat/messages', {
                    message: message,
                    user_name:user_name
                }).then(response => {
                  	console.log(response.data);
                	// this.messages.push(response.data.message);
                });
            }else{
                alert('Error');
            }
        }
    },
});